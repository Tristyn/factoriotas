local Sequence = require("Sequence")
local Waypoint = require("Waypoint")
local BuildOrder = require("BuildOrder")
local CraftOrder = require("CraftOrder")
local ItemTransferOrder = require("ItemTransferOrder")
local MineOrder = require("MineOrder")
local Event = require("Event")

--- SequenceEventAggregator.changed event:
-- This event is invoked every time a Sequence as part of this object is
-- changed, as well as when Waypoint or Order children are changed as well.
-- The SequenceEventAggregator will wrap the event in metadata which describes
-- the type of event it's original sender.  
-- When a populated sequence is added, it's guarenteed to fire `added` events
-- from top to bottom, that is, sequence first, waypoints after. When an object
-- is removed, it's guarenteed to fire `removed` events bottom-up
-- sender :: SequenceEventAggregator: the object which passed on the event.
-- object_name :: string: Can be ["sequences"|"sequence"|"waypoint"|"order"]
-- order_name :: string: Present when object_name is 'order'. Can be ["build_order"|"craft_order"|etc..]
-- inner_event :: table: Contents depends on the value of object_name
-- inner_event fields for each value of object_name:
-- -- "sequences"
-- -- -- type :: string: Can be ["add_sequence"|"remove_sequence"]
-- -- -- sequence :: Sequence
-- -- any other value: see comments in the header of each source file

local SequenceEventAggregator = { }
local metatable = { __index = SequenceEventAggregator }

local ChangeEvent = { }

function SequenceEventAggregator.set_metatable(instance)
	if getmetatable(instance) ~= nil then return end

	setmetatable(instance, metatable)

	Event.set_metatable(instance.changed)
	for _, sequence in ipairs(instance.sequences) do
		Sequence.set_metatable(sequence)
	end
end

function SequenceEventAggregator.new()
	local new = {
		sequences = { },
		changed = Event.new()
	}

	SequenceEventAggregator.set_metatable(new)

	return new
end

function SequenceEventAggregator:new_sequence()
	local sequence = Sequence.new()

	self:_add_sequence(sequence)

	return sequence
end

function SequenceEventAggregator:add_sequence_from_template(sequence_template)
	local sequence = Sequence.new_from_template(sequence_template)
	self:_add_sequence(sequence)
	return sequence
end

function SequenceEventAggregator:_add_sequence(sequence)
	fail_if_missing(sequence)

	if sequence.index ~= nil then
		error("Attempted to add a sequence to the event aggregator when it already has it's index assigned.")
	end


	local sequences = self.sequences
	local insert_index = #sequences + 1
	sequences[insert_index] = sequence
	sequence:set_index(insert_index)

	sequence.changed:add(self, "_on_sequence_changed")

	local event = ChangeEvent.from_sequences_changed(self, "add_sequence", sequence)
	self.changed:invoke(event)
	

	for _, waypoint in ipairs(sequence.waypoints) do
		local waypoint_added_event = sequence:get_change_event("add_waypoint", waypoint)
		self:_on_sequence_changed(waypoint_added_event)
	end
end

function SequenceEventAggregator:remove_sequence(sequence)
	fail_if_missing(sequence)
	
	local sequences = self.sequences
	
	if sequences[sequence.index] ~= sequence then
		error("Sequence was not in this collection")
	end

	sequence.changed:remove(self, "_on_sequence_changed")



	for _, waypoint in ipairs(sequence.waypoints) do
		local event = sequence:get_change_event("remove_waypoint", waypoint)
		self:_on_sequence_changed(event)
	end

	local event = ChangeEvent.from_sequences_changed(self, "remove_sequence", sequence)
	self.changed:invoke(event)
	
	sequence:destroy()
	table.remove(sequences, sequence.index)
	--update sequence indexes
	for i = sequence.index, #sequences do
		sequences[i]:set_index(i)
	end
end

function SequenceEventAggregator:_on_sequence_changed(sequence_event)
	local waypoint = sequence_event.waypoint
	local sequence_event_type = sequence_event.type
	local wrapped_event = ChangeEvent.from_sequence_changed(self, sequence_event)
	

	if sequence_event_type == "add_waypoint" then
		waypoint.changed:add(self, "_on_waypoint_changed")
		self.changed:invoke(wrapped_event)
		self:_process_waypoint_children_with_event(wrapped_event)
	elseif sequence_event_type == "remove_waypoint" then
		waypoint.changed:remove(self, "_on_waypoint_changed")
		self:_process_waypoint_children_with_event(wrapped_event)
		self.changed:invoke(wrapped_event)
	else
		self.changed:invoke(wrapped_event)
	end

end

function SequenceEventAggregator:_process_waypoint_children_with_event(indexer_event)
	assert(indexer_event.object_name == "sequence")
	
	local waypoint = indexer_event.inner_event.waypoint
	local sequence_event_type = indexer_event.inner_event.type

	local order_change_event_type = nil
	if sequence_event_type == "add_waypoint" then
		order_change_event_type = "order_added"
	elseif sequence_event_type == "remove_waypoint" then
		order_change_event_type = "order_removed"
	elseif type(sequence_event_type) == "string" then
		return
	else
		error()
	end

	for order_name, order_group in pairs(waypoint.orders) do
		for _, order in ipairs(order_group) do
			local order_added_event = waypoint:get_order_changed_event(order_change_event_type, order_name, order)
			self:_on_waypoint_changed(order_added_event)
		end
	end
end

function SequenceEventAggregator:_on_waypoint_changed(waypoint_event)

	local event_type = waypoint_event.type
	if event_type == "order_added" then
		local changed_callback_name = SequenceEventAggregator._order_name_to_event_callback[waypoint_event.order_name]
		fail_if_missing(changed_callback_name)
		waypoint_event.order.changed:add(self, changed_callback_name)
	elseif event_type == "order_removed" then
		local changed_callback_name = SequenceEventAggregator._order_name_to_event_callback[waypoint_event.order_name]
		fail_if_missing(changed_callback_name)
		waypoint_event.order.changed:remove(self, changed_callback_name)
	end

	local wrapped_event = ChangeEvent.from_waypoint_changed(self, waypoint_event)
	self.changed:invoke(wrapped_event)
end

function SequenceEventAggregator:_on_build_order_changed(event)
	self:_on_order_changed(event, "build_order")
end

function SequenceEventAggregator:_on_craft_order_changed(event)
	self:_on_order_changed(event, "craft_order")
end

function SequenceEventAggregator:_on_item_transfer_order_changed(event)
	self:_on_order_changed(event, "item_transfer_order")
end

function SequenceEventAggregator:_on_mine_order_changed(event)
	self:_on_order_changed(event, "mine_order")
end

SequenceEventAggregator._order_name_to_event_callback = {
	["build_order"] = "_on_build_order_changed",
	["craft_order"] = "_on_craft_order_changed",
	["item_transfer_order"] = "_on_item_transfer_order_changed",
	["mine_order"] = "_on_mine_order_changed"
}

function SequenceEventAggregator:_on_order_changed(order_event, order_name)
	local wrapped_event = ChangeEvent.from_order_changed(self, order_event, order_name)

	self.changed:invoke(wrapped_event)
end

-- event_type :: string: Can be [add_sequence|remove_sequence]
function ChangeEvent.from_sequences_changed(sender, event_type, sequence_added_or_removed)
	fail_if_missing(sender)
	fail_if_missing(event_type)
	fail_if_missing(sequence_added_or_removed)

	local event = {
		sender = sender,
		object_name = "sequences",
		inner_event = {
			type = event_type,
			sequence = sequence_added_or_removed
		}
	}

	return event
end

function ChangeEvent.from_sequence_changed(sender, sequence_event)
	fail_if_missing(sender)
	fail_if_missing(sequence_event)
	
	local wrapper_event = {
		sender = sender,
		object_name = "sequence",
		inner_event = sequence_event
	}

	return wrapper_event
end

function ChangeEvent.from_waypoint_changed(sender, waypoint_event)
	fail_if_missing(sender)
	fail_if_missing(waypoint_event)
	
	local wrapper_event = {
		sender = sender,
		object_name = "waypoint",
		inner_event = waypoint_event
	}

	return wrapper_event
end

-- order_name can be ["build_order"|"craft_order"|etc...]
function ChangeEvent.from_order_changed(sender, order_event, order_name)
	fail_if_missing(sender)
	fail_if_missing(order_event)
	fail_if_missing(order_name)

	local wrapper_event = {
		sender = sender,
		object_name = "order",
		order_name = order_name,
		inner_event = order_event
	}

	return wrapper_event
end

return SequenceEventAggregator