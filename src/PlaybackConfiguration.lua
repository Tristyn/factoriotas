local util = require("util")
local PlayerControl = require("PlayerControl")

local PlaybackConfiguration = { }
local metatable = { __index = PlaybackConfiguration }

PlaybackConfiguration.mode =
{
    pause = 1,
	play = 2,
	reset = 3
}

function PlaybackConfiguration.set_metatable(instance)
	setmetatable(instance, metatable)
	
	if instance.control_after_playback_completes ~= nil then
		PlayerControl.set_metatable(instance.control_after_playback_completes)
	end
end

function PlaybackConfiguration.new_paused()
	new = {
		mode = PlaybackConfiguration.mode.pause,
		possessed = false
	}

	PlaybackConfiguration.set_metatable(new)

	return new
end

function PlaybackConfiguration.new_reset()
	new = {
		mode = PlaybackConfiguration.mode.reset,
		possessed = false
	}

	PlaybackConfiguration.set_metatable(new)

	return new
end

-- pause_character, num_times_to_step can be nil
-- player is the user that will be controlled to run the sequence.
-- setting num_times_to_step to nil denotes that it will step indefinitely.
function PlaybackConfiguration.new_playing(num_times_to_step)
	fail_if_invalid(player_entity)

	new = 
	{
		possessed = false,

		num_times_to_step = num_times_to_step,
		mode = PlaybackConfiguration.mode.play
	}

	PlaybackConfiguration.set_metatable(new)

	return new
end

function PlaybackConfiguration:get_possessed()
	return self.possessed
end

function PlaybackConfiguration:set_possessed(value)
	fail_if_missing(value)

	if self.possessed == value then
		return self
	end

	local new = util.clone_table(self)

	new.possessed = value
	PlaybackConfiguration.set_metatable(new)

	return new
end

function PlaybackConfiguration:get_player_entity()
	return self.player_entity
end

function PlaybackConfiguration:set_player_entity(player_entity)
	local new = util.clone_table(self)

	new.player_entity = player_entity
	PlaybackConfiguration.set_metatable(new)

	return new
end

function PlaybackConfiguration:get_control_after_playback_completes()
	return self.control_after_playback_completes
end

function PlaybackConfiguration:set_control_after_playback_completes(player_control)
	local new = util.clone_table(self)

	new.control_after_playback_completes = player_control
	PlaybackConfiguration.set_metatable(new)

	return new
end

function PlaybackConfiguration:to_reset()
	local new = util.clone_table(self)

	new.mode = PlaybackConfiguration.mode.reset

	PlaybackConfiguration.set_metatable(new)

	return new
end

function PlaybackConfiguration:to_pause()
	local new = util.clone_table(self)

	new.mode = PlaybackConfiguration.mode.pause

	PlaybackConfiguration.set_metatable(new)

	return new
end

function PlaybackConfiguration:to_play(num_times_to_step)
	local new = util.clone_table(self)
	
	new.num_times_to_step = num_times_to_step
	new.mode = PlaybackConfiguration.mode.play

	PlaybackConfiguration.set_metatable(new)

	return new
end

return PlaybackConfiguration