local PlayerControl = { }
local metatable = { __index = PlayerControl }

function PlayerControl.set_metatable(instance)
	setmetatable(instance, metatable)
end

function PlayerControl.new(controller_type, character_entity)
	if controller_type ~= defines.controllers.ghost or controller_type ~= defines.controllers.god then
		
		if controller_type ~= defines.controllers.character then error() end

	end

	local new = {
		type = defines.controllers.character,
		character = character_entity
	}

	PlayerControl.set_metatable(new)

	return new
end

function PlayerControl.from_player(player_entity)
	fail_if_invalid(player_entity)

	return PlayerControl.new(player_entity.controller_type, player_entity.character)
end

function PlayerControl.from_character(character_entity)
	fail_if_invalid(character_entity)

	return PlayerControl.new(defines.controllers.character, character_entity)
end

-- attach the player to this control.
-- Sets them as ghost if the character entity is invalid.
function PlayerControl:give_player_entity_control(player_entity)
	fail_if_invalid(player_entity)
	
	if self.type == defines.controllers.character
	and is_valid(self.character) == false then
		player_entity.set_controller { type = defines.controllers.ghost }
		return
	end

	player_entity.set_controller(self)
end

function PlayerControl:is_valid()
	if self.type ~= defines.controllers.character then
		return true
	end

	return is_valid(self.character)
end

return PlayerControl