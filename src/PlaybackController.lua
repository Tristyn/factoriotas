local PlaybackConfiguration = require("PlaybackConfiguration")
local Runner = require("Runner")
local PlayerControl = require("PlayerControl")

local PlaybackController = { }
local metatable = { __index = PlaybackController }

PlaybackController.playback_state =
{
    tick_1_prepare_to_attach_runner = 1,
    tick_2_attach_runner = 2,
    tick_3_running = 3,
    tick_4_prepare_to_attach_player = 4,
    tick_5_attach_player = 5
}

function PlaybackController.set_metatable(instance)
    if getmetatable(instance) ~= nil then
        return
    end

    setmetatable(instance, metatable)

    PlaybackConfiguration.set_metatable(instance.configuration)
    if instance.configuration_after_runners_have_stepped ~= nil then
        PlaybackConfiguration.set_metatable(instance.configuration_after_runners_have_stepped)
    end

    for i = 1, #instance.runner_instances do
        Runner.set_metatable(instance.runner_instances[i].runner)
    end
end

function PlaybackController.new()
    local new =
    {
        playback_state = PlaybackController.playback_state.paused,
        num_times_to_step = nil,

        runner_being_stepped_index = nil,
        runner_instances = { },
        
        
        configuration = PlaybackConfiguration.new_paused(),
        configuration_after_runners_have_stepped = nil
    }

    PlaybackController.set_metatable(new)

    new:_apply_configuration(PlaybackConfiguration.new_paused())

    return new
end

-- Create a new runner and character entity
-- Returns nil if sequence is empty
function PlaybackController:new_runner(sequence, controller_type)
    fail_if_missing(sequence)
    if controller_type ~= defines.controllers.character and controller_type ~= defines.controllers.god then
        error()
    end

    if self:get_runner(sequence) ~= nil then
        error()
    end

    local runner_character = controller_type == defines.controllers.character and util.spawn_character() or nil

    local new = 
    {
        runner = Runner.new(sequence),
        player_control = PlayerControl.new(controller_type, runner_character)
    }

    self.runner_instances[#self.runner_instances + 1] = new
end

-- Removes (and murders) the runner.
function PlaybackController:remove_runner(runner)
    fail_if_missing(runner)

    for i=1, #self.runner_instances do
        local instance = self.runner_instances[i]
        if instance.runner == runner then

            -- murder it
            if is_valid(instance.player_control.character) then
                if instance.player_control.character.destroy() == false then
                    log_error( { "TAS-err-generic", "couldn't destroy character :( pls fix" })
                end
            end

            table.remove(self.runner_instances, i)
            return

        end
    end

    error()
end

function PlaybackController:_get_runner_instance(sequence)
    for k, runner_instance in pairs(self.runner_instances) do
        if runner_instance.runner:get_sequence() == sequence then
            return runner_instance
        end
    end
end

function PlaybackController:get_runner(sequence)
    local instance = self:_get_runner_instance(sequence)
    if instance ~= nil then 
        return instance.runner
    end
end

function PlaybackController:try_get_character(sequence)
    local instance = self:_get_runner_instance(sequence)
    if instance ~= nil then
        local character = instance.player_control.character
        return is_valid(character) and character or nil
    end
end

function PlaybackController:runners_exist()
    return #self.runner_instances > 0
end

function PlaybackController:_reset_runners()
    local runner_instances_clone = util.clone_table(self.runner_instances)
    for i = 1, #runner_instances_clone do
        local runner = runner_instances_clone[i].runner
        local sequence = runner:get_sequence()
        self:remove_runner(runner)
        self:new_runner(sequence, runner_instances_clone[i].player_control.type)
    end
end


function PlaybackController:on_tick()
    if self:runners_exist() == false then return end
    if self:is_paused() == true then
        
        if self:_should_possess() == true then
            self:_freeze_player(self.configuration.player_entity)
        end
        return
    end
    

    local player_entity = self.configuration.player_entity
    local runner_properties = self.runner_instances[self.runner_being_stepped_index]
    local runner = runner_properties.runner
    local runner_control = runner_properties.player_control
    if self.playback_state == PlaybackController.playback_state.tick_1_prepare_to_attach_runner then

        self:_freeze_player(player_entity)

        self.playback_state = PlaybackController.playback_state.tick_2_attach_runner

    elseif self.playback_state == PlaybackController.playback_state.tick_2_attach_runner then
        self:_freeze_player(player_entity)
        runner_control:give_player_entity_control(player_entity)
        self:_freeze_player(player_entity)
        player_entity.cheat_mode = false
        self.playback_state = PlaybackController.playback_state.tick_3_running

    elseif self.playback_state == PlaybackController.playback_state.tick_3_running then

        self:_unfreeze_player(player_entity)
        runner:set_player(player_entity)
        runner:step()

        if self.runner_being_stepped_index < #self.runner_instances then
            self.runner_being_stepped_index = self.runner_being_stepped_index + 1
            self.playback_state = PlaybackController.playback_state.tick_1_prepare_to_attach_runner
        else
            if self.configuration_after_runners_have_stepped == nil then
                self.runner_being_stepped_index = 1

                if #self.runner_instances ~= 1 then
                    -- only attach a new player if there are multiple runners to switch between
                    self.playback_state = PlaybackController.playback_state.tick_1_prepare_to_attach_runner
                end

                if self.num_times_to_step ~= nil then
                    self.num_times_to_step = self.num_times_to_step - 1
                    
                    if self.num_times_to_step <= 0 then
                        self.playback_state = PlaybackController.playback_state.tick_4_prepare_to_attach_player
                    end
                end
            else
                -- Get ready to apply the pending configuration.
                self.playback_state = PlaybackController.playback_state.tick_4_prepare_to_attach_player
            end

        end

    elseif self.playback_state == PlaybackController.playback_state.tick_4_prepare_to_attach_player then

        self:_freeze_player(player_entity)

        self.playback_state = PlaybackController.playback_state.tick_5_attach_player

    elseif self.playback_state == PlaybackController.playback_state.tick_5_attach_player then
        
        self:_freeze_player(player_entity)

        if self:_should_possess() == false then
            local player_pause_control = self.configuration.control_after_playback_completes
            player_pause_control:give_player_entity_control(player_entity)
            if player_entity.controller_type == defines.controllers.ghost then
                player_entity.ticks_to_respawn = nil
                -- respawn right now
            end
        end

        self:_freeze_player(player_entity)
        player_entity.cheat_mode = true

        if self.configuration_after_runners_have_stepped ~= nil then
            self:_apply_configuration(self.configuration_after_runners_have_stepped)
        else
            self:_apply_configuration(self.configuration:to_pause())
        end
    end
end

function PlaybackController:_freeze_player(player_entity)

    -- Make character stand still
    if player_entity.character ~= nil then
        player_entity.character.walking_state = { walking = false }
        player_entity.character.character_crafting_speed_modifier = -1
    end

    -- move items from cursor to inventory.
    -- Fixed sometime around 0.15: cursor_stack remains with character when changing controllers 
    --[[if player_entity.cursor_stack.valid_for_read == true then
        player_entity.get_inventory(defines.inventory.player_main).insert(player_entity.cursor_stack)
        player_entity.cursor_stack.clear()
    end--]]
end

function PlaybackController:_unfreeze_player(player_entity)

    if player_entity.character ~= nil then
        player_entity.character.character_crafting_speed_modifier = 0
    end

end

--[Comment]
-- Immediately applies the PlaybackConfiguration.
function PlaybackController:_apply_configuration(config)
    fail_if_missing(config)

    self.configuration_after_runners_have_stepped = nil
    self.configuration = config

    if config.mode == PlaybackConfiguration.mode.pause then
        self.playback_state = nil
        self.num_times_to_step = nil
        self.runner_being_stepped_index = nil
        self:_apply_possession()

    elseif config.mode == PlaybackConfiguration.mode.reset then
        self:_reset_runners()
        self.configuration = config:to_pause()
        self.playback_state = nil
        self.num_times_to_step = nil
        self.runner_being_stepped_index = nil
        self:_apply_possession()
    elseif config.mode == PlaybackConfiguration.mode.play then
        self.playback_state = PlaybackController.playback_state.tick_1_prepare_to_attach_runner
        self.num_times_to_step = config.num_times_to_step
        self.runner_being_stepped_index = 1
    end

end

function PlaybackController:_apply_possession()
    local player_entity = self.configuration.player_entity
    if #self.runner_instances < 1 or is_valid(player_entity) == false then
        return
    end 

    if self:_should_possess() then
        self:_freeze_player(player_entity)
        self.runner_instances[1].player_control:give_player_entity_control(player_entity)
    else
        self.configuration.control_after_playback_completes:give_player_entity_control(player_entity)
    end
end

function PlaybackController:_should_possess()
    local config = self.configuration_after_runners_have_stepped
    if config == nil then
        config = self.configuration
    end

    return config.possessed
end

function PlaybackController:get_configuration(player_index)
    fail_if_missing(player_index)

    if is_valid(self.configuration.player_entity) == false
    or self.configuration.player_entity.index ~= player_index then
        return PlaybackConfiguration.new_paused()
    end

    return self.configuration
end

-- can be nil
function PlaybackController:try_get_desired_configuration(player_index)
    fail_if_missing(player_index)

    if self.configuration_after_runners_have_stepped == nil
    or is_valid(self.configuration_after_runners_have_stepped.player_entity) == false
    or self.configuration_after_runners_have_stepped.player_entity.index ~= player_index then
        return nil
    end
    
    return self.configuration_after_runners_have_stepped
end

function PlaybackController:set_desired_configuration(config)
    fail_if_missing(config)

    if config.mode == PlaybackConfiguration.mode.pause then
        self:_pause(config)
    elseif config.mode == PlaybackConfiguration.mode.play then
        self:_play(config)
    elseif config.mode == PlaybackConfiguration.mode.reset then
        self:_reset(config)
    end
end

function PlaybackController:_reset(config)
    if self:is_paused() then
        self:_apply_configuration(config)
    else
        self.configuration_after_runners_have_stepped = config
    end
end

function PlaybackController:_play(config)
    if self:is_playing() == true then 
    
        self.configuration_after_runners_have_stepped = config
    
    elseif self:is_paused() == true then
        self:_apply_configuration(config)
    else
        self.configuration_after_runners_have_stepped = config
    end
end

--[Comment]
-- Sets the controller to pause as soon as possible.
function PlaybackController:_pause(config)
    if self:is_paused() then
        self:_apply_configuration(config)
        return
    end

    self.configuration_after_runners_have_stepped = config
end

function PlaybackController:is_paused()
     return self.configuration.mode == PlaybackConfiguration.mode.pause
end

function PlaybackController:is_playing()
    return
    self.playback_state == PlaybackController.playback_state.tick_1_prepare_to_attach_runner or
    self.playback_state == PlaybackController.playback_state.tick_2_attach_runner or
    self.playback_state == PlaybackController.playback_state.tick_3_running
end

function PlaybackController:is_player_controlled(player_index)
    fail_if_missing(player_index)

    if self.configuration == nil then
        return false
    end

    -- is this player the one attached to the runner
    local player_entity = self.configuration.player_entity
    if is_valid(player_entity) == false or player_entity.index ~= player_index then
        return false
    end

    -- He's attached, are we playing this tick?
    -- Wanna report 'false' if runner is possessed but playback is paused.
    return self:is_paused() == false
end

function PlaybackController:try_get_pause_control(player_index)
    fail_if_missing(player_index)

    if is_valid(self.configuration.player) == false or
    self.configuration.player.index ~= player_index then
        return nil
    end

    return self.configuration.control_after_playback_completes
end

return PlaybackController