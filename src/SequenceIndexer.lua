-- A fast index of entity -> Waypoint,BuildOrder,etc. Maintains the index of surface entities to
-- their related objects in sequences as objects are modified. 

local Sequence = require("Sequence")
local Waypoint = require("Waypoint")
local MineOrder = require("MineOrder")
local BuildOrder = require("BuildOrder")
local ItemTransferOrder = require("ItemTransferOrder")
local CraftOrder = require("CraftOrder")
local Event = require("Event")
local Set = require("Set")

local SequenceIndexer = {

	
	-- lua prototypes are recreated each time the maps loads and any prototypes
	-- that are used as keys in self._order_indexes are the old instances which were serialized.
	-- It would be nice to use prototypes as keys but tables would have to be rekeyed
	-- during set_metatable, which is difficult when old keys are unknown.
	-- The solution here is to map the prototype to a number enum type which has better persistence properties
	_order_name_to_order_index_key = {
		["mine_order"] = 1,
		["build_order"] = 2,
		["item_transfer_order"] = 3
	}
}
local metatable = { __index = SequenceIndexer }

function SequenceIndexer.set_metatable(instance)
	if getmetatable(instance) ~= nil then
		return
	end

	setmetatable(instance, metatable)
end

function SequenceIndexer.new(sequence_event_aggregator)
	fail_if_missing(sequence_event_aggregator)

	local new = {
		_sequence_event_aggregator = sequence_event_aggregator,
		_waypoint_index = { },

		_order_indexes = {
			["mine_order"] = { },
			["build_order"] = { },
			["item_transfer_order"] = { }
			-- craft orders don't get indexed, they aren't
			-- tied to any entity other than the character
		}
	}

	SequenceIndexer.set_metatable(new)

	sequence_event_aggregator.changed:add(new, "_sequence_event_aggregator_changed_handler")

	return new

end

function SequenceIndexer:_sequence_event_aggregator_changed_handler(aggregator_event)
	fail_if_missing(aggregator_event)

	local object_name = aggregator_event.object_name

	if object_name == "sequence" then
		local sequence_event = aggregator_event.inner_event
		local sequence_event_type = sequence_event.type
		if sequence_event_type == "add_waypoint" then
			self:_add_waypoint_to_index(sequence_event.waypoint)
		elseif sequence_event_type == "remove_waypoint" then
			self:_remove_waypoint_from_index(sequence_event.waypoint)
		end
	elseif object_name == "waypoint" then
		local waypoint_event = aggregator_event.inner_event
		local waypoint_event_type = waypoint_event.type
		if waypoint_event_type == "moved" then
			-- create a dummy with the new changes only to index it 
			local dummy = Waypoint.new(waypoint_event.old_surface_name, waypoint_event.old_position)
			self:_remove_waypoint_from_index(dummy)
			self:_add_waypoint_to_index(waypoint_event.sender)
		elseif waypoint_event_type == "order_added" then
			self:_add_order(waypoint_event.order, waypoint_event.order_name)
		elseif waypoint_event_type == "order_removed" then
			self:_remove_order(waypoint_event.order, waypoint_event.order_name)
		end
	elseif object_name == "order" then

	end

end

function SequenceIndexer:_add_waypoint_to_index(waypoint)
	fail_if_missing(waypoint)
	self._waypoint_index[waypoint:get_entity_id()] = waypoint
end

function SequenceIndexer:_remove_waypoint_from_index(waypoint)
	fail_if_missing(waypoint)
	self._waypoint_index[waypoint:get_entity_id()] = nil
end

function SequenceIndexer:_add_order(order, order_name)
	local indexes = self._order_indexes[order_name]

	if indexes ~= nil then
		local index = order:get_entity_id()
		local orders_for_id = indexes[index]
		if orders_for_id == nil then
			orders_for_id = Set.new()
			indexes[index] = orders_for_id
		end

		if orders_for_id:add(order) == false then
			log_error { "TAS-err-generic", "order added to SequenceIndexer twice, this shouldn't happen." }
		end
	end
end

function SequenceIndexer:_remove_order(order, order_name)
	local indexes = self._order_indexes[order_name]

	if indexes ~= nil then
		local index = order:get_entity_id()
		local orders_for_id = indexes[index]
		if orders_for_id == nil or orders_for_id:remove(order) == false then
			log_error{ "TAS-err-generic", "order removed from SequenceIndexer that wasn't added, this shouldn't happen."}
		end

		if #orders_for_id == 0 then
			indexes[index] = nil
		end
	end
end

function SequenceIndexer:find_waypoint_from_entity(waypoint_entity)
	fail_if_invalid(waypoint_entity)

	local index = Waypoint.id_from_entity(waypoint_entity)
	return self._waypoint_index[index]
end

function SequenceIndexer:find_orders_from_entity(entity, order_name)
	local indexes = self._order_indexes[order_name]

	if indexes == nil then
		error("Order type not supported.")
	end

	local index = util.entity.get_entity_id(entity)

	local orders = indexes[index]

	if orders ~= nil then
		return orders
	else
		return Set.new()
	end
end

function SequenceIndexer:get_sequences()
	return self._sequence_event_aggregator.sequences
end

return SequenceIndexer