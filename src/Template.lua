local util = require("util")

local Template = { }
local metatable = { }

-- source_copy is a verison of source that can have entries added or removed before converting.
-- This is to add support for whitelisting or blacklisting entries. source_copy can be source.
-- source_copy_to_enumerate can be nil here
function Template.convert(source_table, source_copy_to_enumerate)
	fail_if_missing(source_table)

	local seen = { }

	if source_copy_to_enumerate == nil then
		source_copy_to_enumerate = source_table
	end

	return Template.convert_children(source_table, source_copy_to_enumerate, seen)
end

function Template._convert(object, seen)
	return Template.type_handlers[type(object)](object, seen)
end

function Template._convert_table(source, seen)

	local seen_lookup = seen[source]
	if seen_lookup ~= nil then
		return seen_lookup
	end

	if type(source.to_template) == "function" then
		local result = source:to_template(seen)
		seen[source] = result
		-- seen may have been updated with the result earlier if source:to_template called convert_children
		-- but it may have not so set it here as well.
		return result
	else
		return Template.convert_children(source, source, seen)
	end
end

-- source_copy is a verison of source that can have entries added or removed before converting.
-- This is to add support for whitelisting or blacklisting entries. source_copy can be source.
function Template.convert_children(source, source_copy_to_enumerate, seen)


	local target = { }
	seen[source] = target

	for k, v in pairs(source_copy_to_enumerate) do

		local k_template = Template._convert(k, seen)
		local v_template = Template._convert(v, seen)

		target[k_template] = v_template

	end

	return target
end

local function ret(val) return val end
local function void() return end

Template.type_handlers = {
	["table"] = Template._convert_table,
	
	["string"] = ret,
	["number"] = ret,
	["boolean"] = ret,
	
	["nil"] = void,
	["function"] = void,
	["userdata"] = void,
	["thread"] = void,
}

return Template