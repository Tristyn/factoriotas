local Constants = require("Constants")
local Sequence = require("Sequence")
local SequenceEventAggregator = require("SequenceIndexer")

local SequencePorter = { }
local metatable = { __index = SequencePorter }

-- imports and exports sequence state to the sequence indexer or to an object.

function SequencePorter.set_metatable(instance)
	if getmetatable(instance) ~= nil then return end

	setmetatable(instance, metatable)

	SequenceEventAggregator.set_metatable(instance._sequence_event_aggregator)
end

function SequencePorter.new(sequence_event_aggregator)
	fail_if_missing(sequence_event_aggregator)

	local new = {
		_sequence_event_aggregator = sequence_event_aggregator
	}

	SequencePorter.set_metatable(new)

	return new
end

function SequencePorter:export()
	local sequences = self:_get_sequence_templates()

	local header = {
		disclaimer = self:get_v1_disclaimer(),
		format_revision = Constants.current_sequence_file_format_revision,
		sequences = sequences
	}

	return header
end

function SequencePorter:import(sequence_file)
	fail_if_missing(sequence_file)


	if sequence_file.format_revision ~= Constants.current_sequence_file_format_revision then
		-- attempt migration logic here
		
		log_error("Incompatible sequence format version. Please update your mods.", true)
		return
	end

	return sequence_file
end

function SequencePorter:import_to_sequence_event_aggregator(sequence_file)
	imported_sequence_file = self:import(sequence_file)

	self:_replace_sequences_in_sequence_event_aggregator(imported_sequence_file.sequences)
end

function SequencePorter:_replace_sequences_in_sequence_event_aggregator(new_sequence_templates)
	fail_if_missing(new_sequence_templates)

	local sequence_event_aggregator = self._sequence_event_aggregator
	local existing_sequences = util.clone_table(sequence_event_aggregator.sequences)

	for _, sequence in ipairs(existing_sequences) do
		sequence_event_aggregator:remove_sequence(sequence)
	end

	for _, sequence_template in ipairs(new_sequence_templates) do
		local sequence = sequence_event_aggregator:add_sequence_from_template(sequence_template)
		for _, waypoint in ipairs(sequence.waypoints) do
			waypoint:set_highlight(true)
		end
	end
end

function SequencePorter:_get_sequence_templates()
    local templates = { }
    local sequences = self._sequence_event_aggregator.sequences

    for _, sequence in ipairs(sequences) do
        table.insert(templates, sequence:to_template({ }))
    end

    return templates
end

function SequencePorter:get_disclaimer_base()
	return "Feel free to hand-edit and build tools around this data. To " ..
	"deserialize this in a Lua environment simply require() this file."
end

function SequencePorter:get_v1_disclaimer()
	return "File format V1. " .. self:get_disclaimer_base() .. " Note: the waypoint.position field will" ..
	"probably get changed in later versions so don't rely on it existing."
end

return SequencePorter