local SequencePorter = require("SequencePorter")
local GuiEvent = require("GuiEvents")
local Delegate = require("Delegate")

local SequenceImporterView = { }
local metatable = { __index = SequenceImporterView }

function SequenceImporterView.set_metatable(instance)
	if getmetatable(instance) ~= nil then return end

	setmetatable(instance, metatable)
end

function SequenceImporterView.new(container, sequence_porter, gui_events)
	fail_if_invalid(container)
	fail_if_missing(sequence_porter)
	fail_if_missing(gui_events)

	local new = {
		_sequence_porter = sequence_porter, 
		_gui_events = gui_events,
		_root
	}
	
	SequenceImporterView.set_metatable(new)

	new:_initialize_elements(container)

	return new
end

function SequenceImporterView:_initialize_elements(container)
	self._root = container.add { type = "frame", caption = "Import Run", direction = "vertical" }
	
	local file_text_row = self._root.add { type = "flow", direction = "horizontal" }

	file_text_row.add { type = "label", caption = "Run Data:"}
	self._file_text = file_text_row.add { type = "textfield" }

	local buttons_row = self._root.add { type = "flow", direction = "horizontal" }
	--self._overwrite = buttons_row.add { type = "checkbox", caption = "Overwrite Run", state = false }
	self._cancel = buttons_row.add { type = "button", caption = "Cancel", name = util.get_guid() }
	self._gui_events:register_click_callback(self._cancel, Delegate.new(self, "_cancel_clicked_handler"))
	self._import = buttons_row.add { type = "button", caption = "Import", name = util.get_guid() }
	self._gui_events:register_click_callback(self._import, Delegate.new(self, "_import_clicked_handler"))
end

function SequenceImporterView:_import_clicked_handler(event)
	local file_content = self._file_text.text
	local player = game.players[event.player_index]

	if file_content == "" then
		log_error({"TAS-warn-specific", "Importer", "Please copy the content of an exported run into the text box." }, true)
		return
	end

	local ok, parsed_file = serpent.load(file_content)

	if ok == false then
		log_error({"TAS-err-specific", "Importer", "Error loading the run, lua code is invalid.\n" .. err }, true)
		return
	end

	self._sequence_porter:import_to_sequence_event_aggregator(parsed_file)

	self:dispose()
end

function SequenceImporterView:_cancel_clicked_handler(event)
	self:dispose()
end

function SequenceImporterView:show()
	self._root.style.visible = true
end

function SequenceImporterView:hide()
	self._root.style.visible = false
end

function SequenceImporterView:dispose()

	self._gui_events:unregister_click_callbacks(self._cancel, self._import)

	self._root.destroy()
end

return SequenceImporterView